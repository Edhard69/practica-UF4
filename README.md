En aquesta pràctica he creat totes les classes necessàries amb les herències pertinents i amb els seus mètodes. Totes tenen reaprofitament de codi amb sobreescriptures i sobrecarregues, hem utilitzat el polimorfisme i més coses!

La pràctica funciona correctament i que jo sàpiga no m'he deixat cap apartat.

Adjuntament amb el projecte tenim el diagrama UML.
